
(provide 'setup-ecb)

(cond ((< emacs-major-version 25)
       (custom-set-variables '(ecb-options-version "2.40")))
      (t
        (custom-set-variables '(ecb-options-version "2.50"))))

(unless (package-installed-p 'ecb) (package-install 'ecb))

(require 'ecb)

(setq ecb-tip-of-the-day nil
      ecb-layout-name "leftright2"
      ecb-layout-window-sizes nil
      ecb-compile-window-height 0.3
      ecb-layout-window-sizes (quote (("leftright2"
                                       (ecb-directories-buffer-name 0.1 . 0.4)
                                       (ecb-history-buffer-name 0.2 . 0.4)))))

