
(provide 'setup-macosx)

(global-set-key "\M-n" (lambda () (interactive) (insert-string "~")))
(global-set-key "\M-\(" (lambda () (interactive) (insert-string "{")))
(global-set-key "\M-\)" (lambda () (interactive) (insert-string "}")))
(global-set-key "\M-5" (lambda () (interactive) (insert-string "[")))
(global-set-key "\M-0" (lambda () (interactive) (insert-string "]")))
(global-set-key "\M-/" (lambda () (interactive) (insert-string "\\")))

