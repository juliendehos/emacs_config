
(provide 'setup-general)

; shortcuts
(define-key global-map (kbd "C-c C-c") 'comment-region)
(define-key global-map (kbd "C-c c") 'uncomment-region)

; gdb integration
(setq gdb-many-windows t)
(defvar all-gud-modes
  '(gud-mode comint-mode gdb-locals-mode gdb-frames-mode  gdb-breakpoints-mode)
  "A list of modes when using gdb")
(defun kill-all-gud-buffers ()
  "Kill all gud buffers after `q` in Debugger buffer."
  (interactive)
  (let ((count 0))
    (dolist (buffer (buffer-list))
      (with-buffer buffer
                   (when (member major-mode all-gud-modes)
                     (setq count (1+ count))
                     (kill-buffer buffer)
                     (delete-other-windows))) ;fix the remaining two windows issue
      (message "Killed %i buffer(s)." count))))
(defun gud-run-or-stop ()
  "Run or quit gdb (close ecb minor mode if opened)."
  (interactive)
  (when (bound-and-true-p ecb-minor-mode)
    (ecb-minor-mode))
  (if (bound-and-true-p gud-minor-mode)
    (kill-all-gud-buffers)
    (gdb (concat "gdb -i=mi " (read-file-name "Binary file: ")))))

; color scheme
(add-to-list 'custom-theme-load-path "~/.emacs.d/")
(load-theme 'monokai t)

; display column 80
(require 'whitespace)
(setq whitespace-line-column 80)
(setq whitespace-style '(face lines-tail))
(global-whitespace-mode +1)

; display line number
(global-linum-mode t)

; remove start-up message
(setq inhibit-startup-message t)

; disable alarm bell
(setq ring-bell-function 'ignore)

; increase font size
(set-face-attribute 'default (selected-frame) :height 130)

; open multiple files in a single window
(add-hook 'emacs-startup-hook (lambda () (delete-other-windows)) t)

; use y/n instead of yes/no
(defalias 'yes-or-no-p 'y-or-n-p)

; prevent automatic change of default directory
(add-hook 'find-file-hook
          (lambda ()
            (setq default-directory command-line-default-directory)))

; parenthesis highlighting
(show-paren-mode 1)

; go to first error when compiling
(setq compilation-scroll-output 'first-error)

