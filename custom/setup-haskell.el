
(provide 'setup-haskell)

(unless (package-installed-p 'haskell-mode) (package-install 'haskell-mode))

(require 'haskell-mode)


