
(provide 'setup-company)

(unless (package-installed-p 'company) (package-install 'company))

(add-hook 'after-init-hook 'global-company-mode)

; ignore case
(setq company-dabbrev-downcase 0)

; speed-up display (maybe CPU-consuming)
(setq company-idle-delay 0)

; http://company-mode.github.io/

