
; function keys 
(global-set-key (kbd "<f1>") 'flyspell-mode)
(global-set-key (kbd "<f2>") 'previous-buffer)
(global-set-key (kbd "<f3>") 'next-buffer)
(global-set-key (kbd "<f4>") 'previous-error)
(global-set-key (kbd "<f5>") 'next-error)
(global-set-key (kbd "<f6>") 'ecb-minor-mode)
(global-set-key (kbd "<f7>") 'compile)
(global-set-key (kbd "<f8>") 'gud-run-or-stop)
(global-set-key (kbd "<f9>") 'ggtags-find-tag-dwim)
(global-set-key (kbd "<f10>") 'ggtags-prev-mark)
(global-set-key (kbd "<f11>") 'ggtags-create-tags)

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
;(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(package-initialize)
(when (not package-archive-contents) (package-refresh-contents))

(unless (package-installed-p 'use-package) (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(add-to-list 'load-path "~/.emacs.d/custom")
(require 'setup-general)
(require 'setup-ecb)
(require 'setup-haskell)
(require 'setup-company)
(require 'setup-ggtags)

(when (eq system-type 'darwin)
  (require 'setup-macosx))

